import React,{Component} from 'react';
class List extends Component{
    state={
      data:[
            {
                id:"1",
                name:"Meenu",
                age:"24"
            },
            {
                id:"2",
                name:"Radha",
                age:"23"
            },
            {
                id:"3",
                name:"Bindhu",
                age:"23"
            }
        ] 
    };
    render(){
        return(
            <div>
                <h2>List In React</h2>
                <table>
                    <tbody>
                    {this.state.data.map((value,index)=>(
                    <tr key={index}>
                           <td>{value.id}</td>
                           <td>{value.name}</td>
                           <td>{value.age}</td>
                    </tr>
                    ))}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default List;