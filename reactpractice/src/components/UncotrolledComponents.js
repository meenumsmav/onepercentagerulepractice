import React,{Component} from 'react';
class UncontrolledComponent extends Component{
    constructor(props){
        super(props)
        this.state={
            InputOne:'',
            InputTwo:''
        };
        this.InputOne=React.createRef();
        this.InputTwo=React.createRef();
    }
    onSubmit=()=>{
        console.log("Input One: ",this.InputOne.value);
        console.log("Input Two: ",this.InputTwo.value);
    }
    render(){
        return(
            <form>
                <h2>Uncontrolled Component Form</h2>
                <input type="text" name="InputOne" ref={input=>(this.InputOne=input)}/>
                <input type="text" name="InputTwo" ref={input=>(this.InputTwo=input)}/>
                <button type="button" onClick={this.onSubmit}>Submit</button>
            </form>
        )
    }
}
export default UncontrolledComponent;