import React,{ Component } from 'react';
class Counter extends Component{
   state={
       count:0
   }
    Increment = () =>{
        this.setState({
            count:this.state.count+1
        });
    }
    Decrement = () =>{
        this.setState({
            count:this.state.count-1
        });
    }
    render(){
        return(
          <div>
              <h2>Counter Component</h2>
              <h1>{this.state.count}</h1>
              <button type="button" onClick={this.Increment} >+</button>
              <button type="button" onClick={this.Decrement} >-</button>
          </div>
        )
    }
}
export default Counter;