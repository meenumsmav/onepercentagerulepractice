import React,{Component} from 'react';
class TodayTest extends Component{
    state={
        FirstName:'',
        LastName:''
    };
    handleChange = (event)=>{
    this.setState({
        [event.target.name]:event.target.value
    });
    }
    onSubmit = ()=>{
        console.log("Output:",this.state);
    }
    render(){
        return(
            <form>
                <h2>Test Form</h2>
                <h1>{this.state.FirstName}{this.state.LastName}</h1>
                <input type="text" name="FirstName" value={this.state.FirstName} onChange={this.handleChange}/>
                <input type="text" name="LastName" value={this.state.LastName} onChange={this.handleChange}/>
                <button type="button" onClick={this.onSubmit}>Submit</button>
            </form>
        )
    }
}
export default TodayTest;