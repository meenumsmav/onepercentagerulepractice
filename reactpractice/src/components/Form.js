import React,{Component} from 'react';
class Form extends Component{
    state={
        firstName:'',
        lastName:''
    };
    handleChange = (event)=>{
        this.setState({
            [event.target.name]:event.target.value
        });
        
    }
    onSubmit = (event)=>{
        event.preventDefault();
        console.log(this.state);
    }
    render(){
        return(
            <form>
                <h2>Form Component</h2>
                <input type="text" name="firstName" value = {this.state.firstName} onChange={this.handleChange}/>
                <input type="text" name="lastName" value = {this.state.lastName} onChange={this.handleChange}/>
                <button onClick={this.onSubmit}>Submit</button>
            </form>
        )
    }
}
export default Form;