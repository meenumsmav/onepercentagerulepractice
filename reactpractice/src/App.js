import React from 'react';
import './App.css';
//import Counter from './components/Counter';
//import Form from './components/Form';
//import UncontrolledComponent from './components/UncotrolledComponents';
//import TodayTest from './components/TodayTest';
import List from './components/List';
function App() {
  return (
    <div className="App">
        {/* <Counter/>*/}
        {/*<Form/>*/}
        {/*<TodayTest/>*/}
        {/*<UncontrolledComponent/>*/}
        <List/>
    </div>
  );
}

export default App;
