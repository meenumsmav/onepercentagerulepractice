import React,{Component} from 'react';
import "./TodoApp.css";
class TodoApp extends Component{
    state={
        Input:'',
        Items:[]
    }
    handleChange = (event)=>{
        this.setState({
            Input : event.target.value
        });
    };
    storeItems = event =>{
        event.preventDefault();
        const {Input} = this.state;
        /*const allItems = this.state.Items;
        allItems.push(Input);
        this.setState({
            Items : allItems
        });*/
        this.setState({
            Items : [...this.state.Items,Input],
            Input:''
        });
    };
    deleteItem = (key) =>{
       /* const allItems = this.state.Items;
        allItems.splice(key,1);
        this.setState({
            Items:allItems
        });*/
        this.setState({
            Items : this.state.Items.filter((data,index)=> index !== key)
        });
    };
    render(){
        const{Input,Items} = this.state;
       
        return(
            <div className="todoapp-container">
                <form className="input-section" onSubmit={this.storeItems}>
                    <h1>To Do App</h1>
                    <input type="text" value={Input} onChange={this.handleChange} placeholder="Enter items...."/>
                </form>
                <ul>
                  
                    {Items.map((data,index) => (
                        <li key={index}>{data}<i className="fas fa-trash-alt" onClick={()=>this.deleteItem(index)}></i></li>
                    ))}
                </ul>
            </div>
        )
    }
}
export default TodoApp;